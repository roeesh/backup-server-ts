import log from "@ajar/marker";
import { exec } from "child_process";
import cron from "node-cron";
import net from "net";
import databases from "../config.json";


const { port } = process.env;
const dbObject = JSON.parse(JSON.stringify(databases));

// const server = net.createServer();
const socket = net.createConnection({port: Number(port) },()=>{
  log.yellow("✨ ⚡connected to saver! ⚡✨");
});
socket.on("data", buffer => {
  log.v("Saver: ",buffer.toString());
  for (const db in dbObject) {
    log.blue("-> Sent file:", db);
    let dumpFileName = `${db}DB`;
    
    // every 2 minute
    cron.schedule("*/2 * * * *", () => {
      // console.log("running a task every 2 minutes");
      dumpFileName += `${Date.now()}`;
      const task = exec(
        `docker exec mysql-db /usr/bin/mysqldump -u root --password=qwerty ${db}`
      );
      task.stdout?.on("data", data => {
        socket.write(`${ JSON.stringify( { data: data, fileName: dumpFileName} ) }\n`);
      });
    });
  }
});

socket.on("end", () => {
    log.yellow("✨ ⚡disconnected from saver ⚡✨");
});